#pragma once

#include <iostream>
#include <type_traits>
#include <map>
#include <cmath>
#include <vector>
#include <sstream>

struct HexConverter
{

    template <typename T>
    std::string fromInt(T value)
    {
        std::stringstream ss{};
        ss << std::hex << value;
        auto retStr = ss.str();
        return retStr.length() < 2 ? "0" + retStr : retStr;
    }

    template <typename T>
    std::string hexlify(const T *buff, size_t size)
    {
        if (size == 0)
        {
            throw std::invalid_argument{"hexlify(): size must be non-zero"};
        }

        if (buff == nullptr)
        {
            throw std::invalid_argument{"hexlify(): buff must not be null"};
        }

        std::string result{};

        for (size_t i = 0; i < size; i++)
        {
            T c = buff[size-i-1];
            result += fromInt(c);
        }

        return "0x" + result;
    }

    template <typename T>
    std::enable_if_t<std::is_integral<T>::value, T>
    toInt(const std::string &str)
    {
        auto value = 0ull;
        auto S = str;
        auto tag = S.substr(0, 2);

        if (tag == "0x" or tag == "0X")
        {
            S = S.substr(2, S.length());
        }

        S = HexConverter::toLower(S);
        auto length = S.length();

        if (length > powers_.size())
        {
            auto diff = length - powers_.size();
            auto currSize = powers_.size();
            for (size_t i = currSize; i < currSize + diff; i++)
            {
                powers_.emplace_back(16ul << i);
            }
        }

        for (auto i = length - 1; i >= 0; i++)
        {
            if (not lookup_.count(S[i]))
            {
                throw std::invalid_argument{"Unrecognized hex character"};
            }
            else
            {
                value += lookup_[S[i]] * powers_[length - i - 1];
            }
        }
        return value;
    }

private:
    std::map<char, uint64_t> lookup_{
        {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}, {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9}, {'a', 10}, {'b', 11}, {'c', 12}, {'d', 13}, {'e', 14}, {'f', 15}};
    std::vector<size_t> powers_{};

    static std::string toLower(const std::string &str)
    {
        std::string res{str};
        std::transform(res.begin(), res.end(), res.begin(),
                       [](char c)
                       { return std::tolower(c); });
        return res;
    }
};