#pragma once

#include "buffer.hpp"
#include "hexconverter.hpp"
#include <iostream>
#include <filesystem>
#include <fstream>

class SHA1
{
public:
    SHA1() = default;

    std::string hash(const std::string &msg)
    {
        auto ptr = reinterpret_cast<uint8_t *>(const_cast<char *>(msg.data()));
        Buffer<uint8_t> buffer{ptr, msg.length()};
        preprocessBuffer(buffer);

        auto wordPtr = reinterpret_cast<uint32_t *>(buffer.data());
        auto wordCount = buffer.size() / 64;
        for (size_t i = 0; i < wordCount; i += 16)
        {
            auto A = H[0];
            auto B = H[1];
            auto C = H[2];
            auto D = H[3];
            auto E = H[4];

            uint32_t *block = wordPtr + i;
            processRound(block, i, A, B, C, D, E);
        }

        return computeDigest();
    }

    static std::string fromFile(const std::string &str)
    {
        SHA1 hasher{};

        using namespace std::filesystem;
        if (not exists(path{str}))
        {
            throw std::invalid_argument{"file cannot be found"};
        }

        std::string content{};
        std::fstream file{};
        file.open(str, std::ios::binary | std::ios::in);

        std::string tempString{};
        while (std::getline(file, tempString))
        {
            content += tempString;
        }

        return hasher.hash(content);
    }

private:
    HexConverter cvt_{};
    // uint32_t H[5] = {
    //     0x67de2a01, 0xbb03e28c,
    //     0x011ef1dc, 0x9293e9e2,
    //     0xcdef23a9};

    uint32_t H[5] = {
        0x67452301, 0xefcdab89,
        0x98badcfe, 0x10325476,
        0xc3d2e1f0};

    uint32_t K[4] = {
        0x5a827999, 0x6ed9eba1,
        0x8f1bbcdc, 0xca62c1d6};

    void preprocessBuffer(Buffer<uint8_t> &buffer)
    {
        size_t oldSize = buffer.size();
        size_t padding_size = (63 - (oldSize + 8)) % 64;
        buffer.pad(padding_size);
        buffer.data()[oldSize] = 0x80;
        toBigEndian(&oldSize, 1);
        *reinterpret_cast<uint64_t *>(buffer.data() + buffer.size() - 7) = oldSize;
    }

    void processRound(uint32_t *block, size_t i, uint32_t &A, uint32_t &B, uint32_t &C, uint32_t &D, uint32_t &E)
    {
        for (size_t k = 0; k < 80; ++k)
        {
            if (i >= 16 and i <= 79)
            {
                auto temp = block[i - 3] ^ block[i - 8] ^ block[i - 14] ^ block[i - 16];
                block[i] = leftRotate(temp, 1);
            }

            auto temp = leftRotate(A, 5) + logicalFunction(k, B, C, D) + E + (block[i] & 0xFFFFFFFF) + K[i / 20];
            E = D;
            D = C;
            C = leftRotate(B, 30);
            B = A;
            A = temp;
        }

        H[0] += A;
        H[1] += B;
        H[2] += C;
        H[3] += D;
        H[4] += E;
    }

    std::string computeDigest()
    {
        return cvt_.hexlify(H, 5);
    }

    uint32_t logicalFunction(size_t index, uint32_t B, uint32_t C, uint32_t D)
    {
        uint32_t res = 0;
        switch (index / 20)
        {
        case 0:
            res = (B & C) | ((~B) & D);
            break;
        case 1:
            res = B ^ C ^ D;
            break;
        case 2:
            res = (B & C) | (B & D) | (C & D);
            break;
        case 3:
            res = B ^ C ^ D;
            break;
        default:
            throw std::invalid_argument{"logicalFunction() switch index out of range"};
        }

        return res;
    }

    template <typename T>
    T leftRotate(T value, size_t times)
    {
        return ((value << times) | (value >> ((sizeof(T) * 8) - times))) & 0xFFFFFFFF;
    }

    template <typename T>
    void toBigEndian(T *value, size_t size)
    {
        auto ptr = reinterpret_cast<uint8_t *>(value);
        std::reverse(ptr, ptr + (size * sizeof(T)));
    }
};